﻿using MoviesAPI.Models;
using MoviesAPI.Models.ThirdParty;
using MoviesAPI.Services;
using MoviesAPI.Services.ThidParty;
using MoviesAPI.Services.ThirdParty.Mappers;
using NSubstitute;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace MoviesAPITest.ServicesTest
{
    public class EnrichMovieTest
    {
        [Fact]
        public void ReturnMoviesWithTrailer()
        {
            //arrange
            var mockEnrich = Substitute.For<IEnrichMovie>();
            var mockThirdPartyCall = Substitute.For<IThirdPartyCalls>();
            var mockMapperYTB = Substitute.For<IMapperYTB>();

            var modelExpect = new MoviesAPI.Models.MovieModelList
            {
                Search = new List<MoviesAPI.Models.MovieModel>()
                {
                    new MovieModel(){  Title="Cars", Year="2006", Description=null, Trailer="" }
                }
            };

            var parameters = string.Format("q={0}", string.Concat(modelExpect.Search.FirstOrDefault().Title, string.Empty, modelExpect.Search.FirstOrDefault().Year, string.Empty, "Trailer"));
            var request=mockThirdPartyCall.CallYouTube(parameters);
            var modelYTBExpect = new YouTubeModel() { items = new List<YouTubeItem>() { new YouTubeItem() { id = new YouTubeId() { videoId = "SbXIj2T-_uk" } } } };
            mockMapperYTB.MapperRequest(request).Returns(modelYTBExpect);
            var stringTrailer = string.Format("https://www.youtube.com/watch?v={0}", modelYTBExpect.items.FirstOrDefault().id.videoId);

            //act
            var model = new EnrichMovie(mockThirdPartyCall,mockMapperYTB).EnrichWithtrailer(modelExpect);

            //assert
            Assert.True(model.Search.FirstOrDefault().Trailer == stringTrailer);
        }
        [Fact]
        public void ReturnMoviesWithoutTrailer()
        {
            //arrange
            var mockEnrich = Substitute.For<IEnrichMovie>();
            var mockThirdPartyCall = Substitute.For<IThirdPartyCalls>();
            var mockMapperYTB = Substitute.For<IMapperYTB>();

            var modelExpect = new MoviesAPI.Models.MovieModelList
            {
                Search = new List<MoviesAPI.Models.MovieModel>()
                {
                    new MovieModel(){  Title="Cars", Year="2006", Description=null, Trailer="" }
                }
            };

            var parameters = string.Format("q={0}", string.Concat(modelExpect.Search.FirstOrDefault().Title, string.Empty, modelExpect.Search.FirstOrDefault().Year, string.Empty, "Trailer"));
            var request = mockThirdPartyCall.CallYouTube(parameters);
            var modelYTBExpect = new YouTubeModel();
            mockMapperYTB.MapperRequest(request).Returns(modelYTBExpect);
           
            //act
            var model = new EnrichMovie(mockThirdPartyCall, mockMapperYTB).EnrichWithtrailer(modelExpect);

            //assert
            Assert.True(model.Search.FirstOrDefault().Trailer == string.Empty);
        }

    }
}
