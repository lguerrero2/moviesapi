﻿using MoviesAPI.Models;
using MoviesAPI.Services;
using MoviesAPI.Services.ThidParty;
using MoviesAPI.Services.ThirdParty.Mappers;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MoviesAPITest.ServicesTest
{
    public class MovieServiceTest
    {
        [Theory]
        [InlineData("Cars",1)]
        public void ReturnValidModel_WhenHaveTitleAndPage(string title, int page)
        {
            //arrange
            var mockMapper = Substitute.For<IMapperOmdb>();
            var mockThirdParty = Substitute.For<IThirdPartyCalls>();
            var parameters = string.Format("s={0}&page={1}", title, page);
            var result = "{movie list}";

            var modelExpect = new MoviesAPI.Models.MovieModelList
            {
                Search = new List<MoviesAPI.Models.MovieModel>()
                {
                    new MovieModel(){  Title="Movie Test", Year="2019", Description="test", Trailer="http://..." }
                }
            };

            mockThirdParty.CallOmdb(parameters).Returns(result);
            mockMapper.SetMovieModelList(result).Returns(modelExpect);

            //act
            var model = new MovieService(mockMapper, mockThirdParty).GetMovies(title,page);
            //assert
            Assert.True(model.Search.FirstOrDefault().Title== modelExpect.Search.FirstOrDefault().Title);
            Assert.True(model.Search.FirstOrDefault().Year == modelExpect.Search.FirstOrDefault().Year);
            Assert.True(model.Search.FirstOrDefault().Trailer == modelExpect.Search.FirstOrDefault().Trailer);
            Assert.True(model.Search.FirstOrDefault().Description == modelExpect.Search.FirstOrDefault().Description);
        }
    }
}
