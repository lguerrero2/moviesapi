﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using MoviesAPI.Models;
using MoviesAPI.Services;
using MoviesAPI.Services.ThirdParty.Mappers;
using NSubstitute;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace MoviesAPITest.ServicesTest
{

    public class MapperJsonMoviesTest
    {
        [Fact]
        public void ReturnValidMovie_WhenJsonIsValid()
        {
            //arrange
            var mockUtil = Substitute.For<IUtil>();  
            var modelExpect = new MovieModelList
            {
                Search = new List<MovieModel>()
                {
                    new MovieModel(){  Title="Cars", Year="2006", Description=null, Trailer="https://www.youtube.com/watch?v=SbXIj2T-_uk" }
                }
            };

            var fixture = new Fixture();
            fixture.Customize(new AutoConfiguredNSubstituteCustomization());

            var mockEnrich = fixture.Freeze<IEnrichMovie>();
            mockUtil.Deserialize<MovieModelList>("{}").Returns(modelExpect);
            mockEnrich.EnrichWithtrailer(modelExpect).Returns(modelExpect);

            //act
            var model = new MapperOmdb(mockEnrich, mockUtil).SetMovieModelList("{}");
            //assert
            Assert.True(model.Search.FirstOrDefault().Title == modelExpect.Search.FirstOrDefault().Title);
            Assert.True(model.Search.FirstOrDefault().Year == modelExpect.Search.FirstOrDefault().Year);
            Assert.True(model.Search.FirstOrDefault().Trailer == modelExpect.Search.FirstOrDefault().Trailer);
            Assert.True(model.Search.FirstOrDefault().Description == modelExpect.Search.FirstOrDefault().Description);
        }
    }
}
