﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MoviesAPI.Exceptions;
using MoviesAPI.Models;
using MoviesAPI.Services;

namespace MoviesAPI.Controllers
{
    [Route("api/")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieServices _service;

        public MoviesController(IMovieServices service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("Movies")]
        public ActionResult<MovieModelList> Movies([Required] string title, [Required] int page)
        {
            try
            {
                if (MovieModel.IsValidPage(page))
                {
                    var result = _service.GetMovies(title, page);
                    if (result.Search.Count() == 0)
                    {
                        return NotFound();
                    }
                    return result;
                }
                else
                {
                    return BadRequest("Valid page between 1-100");
                }
            }
            catch(NotFoundMovieException ex)
            {
                return StatusCode(404, ex);
            }
            catch (Exception ex)
            {
                var error = ex;
                return StatusCode(500, ex.Message);
            }           
        }
    }
}