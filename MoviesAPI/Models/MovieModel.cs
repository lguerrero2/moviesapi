﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class MovieModel
    {
        public string Title { get; set; }

        public string Year { get; set; }

        public string Poster { get; set; }

        public string Trailer { get; set; }

        public int Page { get; set; }

        public static bool IsValidPage(int page)
        {
            if (page > 0 && page <= 100)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
