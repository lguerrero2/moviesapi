﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class MovieModelList
    {
        public List<MovieModel> Search { get; set; }
    }
}
