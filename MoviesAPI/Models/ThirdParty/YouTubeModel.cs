﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models.ThirdParty
{
    public class YouTubeModel
    {
        public List<YouTubeItem> items { get; set; }
    }
}
