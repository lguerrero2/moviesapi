﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models.ThirdParty
{
    public class YouTubeItem
    {
        public YouTubeId id { get; set; }
    }

    public class YouTubeId
    {
        public string videoId { get; set; }
    }
}
