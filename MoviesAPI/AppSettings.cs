﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI
{
    public class AppSettings
    {
        public string EndpointOMDB { get; set; }

        public string OmdbKey { get; set; }

        public string EndPointYouTube { get; set; }

        public string YouTubeKey { get; set; }

    }
}
