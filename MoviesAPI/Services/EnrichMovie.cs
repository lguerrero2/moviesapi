﻿using MoviesAPI.Models;
using MoviesAPI.Services.ThidParty;
using MoviesAPI.Services.ThirdParty.Mappers;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services
{
    public class EnrichMovie : IEnrichMovie
    {
        private readonly IThirdPartyCalls _thirdPartyCalls;
        private readonly IMapperYTB _mapperYTB;

        public EnrichMovie(IThirdPartyCalls thirdPartyCalls, IMapperYTB mapperYTB)
        {
            _thirdPartyCalls = thirdPartyCalls;
            _mapperYTB = mapperYTB;
        }

        public MovieModelList EnrichWithtrailer(MovieModelList movies)
        {
            Parallel.ForEach(movies.Search, (currentMovie) =>
            {
                currentMovie.Trailer = GetTrailer(currentMovie.Title, currentMovie.Year);
            });

            return movies;
        }

        private string GetTrailer(string title, string year)
        {
            var parameters = string.Format("q={0}", string.Concat(title, string.Empty,year,string.Empty,"Trailer"));
            var request = _thirdPartyCalls.CallYouTube(parameters);
            var result = _mapperYTB.MapperRequest(request);

            if (result.items != null)
            {
                var videoItem = result.items.FirstOrDefault();
                if (videoItem != null && videoItem.id != null && !string.IsNullOrEmpty(videoItem.id.videoId))
                {

                    return string.Format("https://www.youtube.com/watch?v={0}", videoItem.id.videoId);
                }
            }
            return string.Empty;
        }

    }
}
