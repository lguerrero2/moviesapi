﻿using MoviesAPI.Models;
using MoviesAPI.Services.ThidParty;
using MoviesAPI.Services.ThirdParty.Mappers;

namespace MoviesAPI.Services
{
    public class MovieService : IMovieServices
    {
        private readonly IMapperOmdb _mapperService;
        private readonly IThirdPartyCalls _thirdPartyCalls;

        public MovieService(IMapperOmdb mapperService, IThirdPartyCalls thirdPartyCalls)
        {
            _mapperService = mapperService;
            _thirdPartyCalls = thirdPartyCalls;
        }

        public MovieModelList GetMovies(string title, int page)
        {
            var parameters = string.Format("s={0}&page={1}",title, page);
            var result = _thirdPartyCalls.CallOmdb(parameters);
            var movies = _mapperService.SetMovieModelList(result);
            return movies;
        }
    }
}
