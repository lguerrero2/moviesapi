﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services
{
    public interface ICache
    {
        object GetValue(string key);

        bool Add(string key, object value, DateTimeOffset absExpiration);

        void Delete(string key);
    }
}
