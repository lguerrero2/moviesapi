﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoviesAPI.Models;

namespace MoviesAPI.Services
{
    public interface IMovieServices
    {
        MovieModelList GetMovies(string title, int page);
    }
}
