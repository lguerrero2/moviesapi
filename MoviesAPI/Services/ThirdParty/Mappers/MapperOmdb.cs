﻿using MoviesAPI.Exceptions;
using MoviesAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThirdParty.Mappers
{
    public class MapperOmdb: IMapperOmdb
    {
        private readonly IEnrichMovie _enrichService;

        private readonly IUtil _util;

        public MapperOmdb(IEnrichMovie enrichMovie, IUtil util)
        {
            _enrichService = enrichMovie;
            _util = util;
        }
        
        public MovieModelList SetMovieModelList(string result)
        {
            var movies = _util.Deserialize<MovieModelList>(result);

            if (movies!=null && movies.Search == null)
            {
                throw new NotFoundMovieException() { Message="Movie not found"};
            }

            movies = _enrichService.EnrichWithtrailer(movies);
            return movies;
        }
    }
}
