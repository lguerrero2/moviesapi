﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThirdParty.Mappers
{
    public class Util : IUtil
    {
        public T Deserialize<T>(string json)
        {
            var objectResult = JsonConvert.DeserializeObject<T>(json);
            return objectResult;

        }

    }
}
