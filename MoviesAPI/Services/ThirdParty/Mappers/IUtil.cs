﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThirdParty.Mappers
{
    public interface IUtil
    {
        T Deserialize<T>(string json);
    }
}
