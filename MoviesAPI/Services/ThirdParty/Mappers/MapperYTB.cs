﻿using MoviesAPI.Models.ThirdParty;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThirdParty.Mappers
{
    public class MapperYTB: IMapperYTB
    {
        private readonly IUtil _util;

        public MapperYTB(IUtil util)
        {
            _util = util;
        }

        public YouTubeModel MapperRequest(string request)
        {
            var result = _util.Deserialize<YouTubeModel>(request);
            return result;
        }
    }
}
