﻿using MoviesAPI.Models.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThirdParty.Mappers
{
    public interface IMapperYTB
    {
        YouTubeModel MapperRequest(string request);

    }
}
