﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThidParty
{
    public interface IThirdPartyCalls
    {
        string CallOmdb(string url);
        string CallYouTube(string parameters);
    }
}
