﻿using Microsoft.Extensions.Options;
using MoviesAPI.Exceptions;
using MoviesAPI.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MoviesAPI.Services.ThidParty
{
    public class ThirdPartyCalls : IThirdPartyCalls
    {
        private readonly ICache _cacheService;
        private readonly IOptions<AppSettings> _settings;

        public ThirdPartyCalls(ICache cacheService, IOptions<AppSettings> settings)
        {
            this._cacheService = cacheService;
            this._settings = settings;

        }

        public string CallOmdb(string parameters)
        {
            var url = string.Concat(_settings.Value.EndpointOMDB, parameters); 
            return Call(url);
        }

        public string CallYouTube(string parameters)
        {

            var url = string.Concat(_settings.Value.EndPointYouTube, parameters);
           // var url2 = string.Format("https://www.googleapis.com/youtube/v3/search?q={0}&part=snippet&type=video&key={1}",title,key);
            return Call(url);
        }

        private string Call(string url)
        {
            var cacheObject = _cacheService.GetValue(url);

            if (cacheObject != null)
            {
                return cacheObject.ToString();
            }

            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            string responseString;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        responseString = reader.ReadToEnd();
                        _cacheService.Add(url, responseString, DateTimeOffset.UtcNow.AddHours(8));
                        return responseString;
                    }
                }
            }
            else
            {
                throw new NotFoundMovieException();
            }
        }

    }
}
