﻿using MoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Services
{
    public interface IEnrichMovie
    {
        MovieModelList EnrichWithtrailer(MovieModelList movies);
    }
}
