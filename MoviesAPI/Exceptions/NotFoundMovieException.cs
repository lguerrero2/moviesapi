﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Exceptions
{
    public class NotFoundMovieException: Exception
    {
        public string Message { get; set; }
    }
}
