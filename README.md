# MoviesAPI

**Bitbucket Repository: https://bitbucket.org/lguerrero2/moviesapi/src/master/**
To run the application please clone this repository and follow the next steps:


These are the sources for populating colors external service. To run please follows.

1. Install and run **`docker`** https://www.docker.com/get-started
2. Go to 'moviesapi' directory.
3. Run command: **'docker build -t Movies .'**
4. Run command: **'docker images'** and confirm the image 'Movies' was created.
5. Run command: **'docker run -d -p 8000:80 Movies'** 
6. Run command: **'docker ps'** and confirm the service container is up and runing.

If you want, you can consult the API directly using the next URL https://localhost:8000/api/movies?title=cars&page=1

Also you can access to the documentation in the next URL http://localhost:8000/swagger/index.html


